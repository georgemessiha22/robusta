from encryption import (ShiftBasedEcryption, ReverseEcryption, MatrixEcryption)

if __name__ == "__main__":
    while True:
        try:
            print("Please insert message (Press <Enter> for new line and Double <Enter> for processing): ")
            multiline = []
            while True:
                line = input()
                if line:
                    multiline.append(line)
                else:
                    break
            value = '\n'.join(multiline)

            selection = int(input("Please insert the Encryption Style number:"
                                  "\n\t1.Shift Based"
                                  "\n\t2.Matrix Encryption"
                                  "\n\t3.Reverse Encryption\n"))
            encryption = None
            if selection == 1:
                encryption = ShiftBasedEcryption()
            elif selection == 2:
                encryption = MatrixEcryption()
            if selection == 3:
                encryption = ReverseEcryption()
            else:
                raise Exception("Please enter valid Choice!!")

            selection = int(input("Please Choose: \n\t1.encrypt \n\t2.decrypt\n"))
            if selection == 1:
                print("Result: \n" + encryption.encode(message=value))
            elif selection == 2:
                print("Result: \n" + encryption.decode(encrypted_message=value))
            else:
                raise Exception("Please enter valid Choice!!")

            value = input("Try Again [y]: ")
            if value and value != "y" and value != "Y" and value != "Yes" and value != "YES" and value != "yes":
                break
            else:
                print("\n\n\n\n")
        except ValueError as e:
            print("Please enter valid Choice!!")
            print("try again ...\n\n\n\n")

        except Exception as e:
            print(e.args[0])
            print("try again ...\n\n\n\n")
