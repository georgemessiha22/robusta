import unittest

from encryption.reverse_encryption import ReverseEcryption


class TestReverseEncryption(unittest.TestCase):
    def test_encode_decode(self):
        """
        Testing Reverse Base Encryption encoding vs decode
        """
        enc = ReverseEcryption()
        msg = "Hello World"
        encrypted_msg = enc.encode(msg)
        self.assertEqual(msg, enc.decode(encrypted_message=encrypted_msg))
