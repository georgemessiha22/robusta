import unittest

from encryption.matrix_encryption import MatrixEcryption


class TestMatrixEncryption(unittest.TestCase):
    def test_encode_decode(self):
        """
        Testing Matrix Base Encryption encoding vs decode
        """
        enc = MatrixEcryption()
        msg = "Hello World"
        encrypted_msg = enc.encode(msg)
        self.assertEqual(msg, enc.decode(encrypted_message=encrypted_msg))
