import unittest

from encryption.shift_based import ShiftBasedEcryption


class TestShiftBasedEncryption(unittest.TestCase):
    def test_encode(self):
        """
        Testing Shift Base Encryption encoding
        """
        enc = ShiftBasedEcryption()
        e = enc.encode("Hello World")
        self.assertEqual(e, "Khoor Zruog")

    def test_decode(self):
        """
        Testing Shift Base Encryption encoding
        """
        enc = ShiftBasedEcryption()
        e = enc.decode("Khoor Zruog")
        self.assertEqual(e, "Hello World")
