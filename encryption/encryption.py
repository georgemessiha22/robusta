import abc


class Encryption(abc.ABC):
    def __init__(self, message: str = None, encrypted_message: str = None):
        self.msg = message
        self.encrypted = encrypted_message

    @abc.abstractmethod
    def encode(self, message: str = None) -> str:
        """
        Encrypt message
        :param message: message to be encrypted
        :return: encrypted message
        """
        raise NotImplementedError("encode method must be implemented.")

    @abc.abstractmethod
    def decode(self, encrypted_message: str = None) -> str:
        """
        Decrypt Message
        :param encrypted_message: decrypt the encrypted message
        :return: plain message
        """
        raise NotImplementedError("decode method must be implemented.")

    def get_message(self) -> str:
        """
        Get Message
        :return: message
        """
        return self.msg

    def set_message(self, value: str):
        """
        Set message
        :param value: target message
        """
        self.msg = value

    def get_encrypted_message(self) -> str:
        """
        Get Encrypted message
        :return: encrypted message
        """
        return self.encrypted

    def set_encrypted_message(self, value: str):
        """
        Set encrypted message
        :param value: encrypted message
        """
        self.encrypted = value
