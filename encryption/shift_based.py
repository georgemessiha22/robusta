from .encryption import Encryption

'''
Given a string shift every character by 3 characters so if word contains letter “A” in the
encrypted string, “A” will be replaced by “D”.
    - Example input : “Hello World”
    - Expected output : “Khoor Zruog”
'''


class ShiftBasedEcryption(Encryption):
    def encode(self, message: str = None) -> str:
        """
        Encrypt message
        :param message: message to be encrypted
        :return: encrypted message
        """
        if message:
            self.msg = message
        self.encrypted = ""
        for i in self.msg:
            if i != '\n' and i != ' ':
                self.encrypted += chr(ord(i) + 3)
            elif i == '\n':
                self.encrypted += "\n"
            elif i == ' ':
                self.encrypted += " "
        return self.encrypted

    def decode(self, encrypted_message: str = None) -> str:
        """
        Decrypt Message
        :param encrypted_message: decrypt the encrypted message
        :return: plain message
        """
        if encrypted_message:
            self.encrypted = encrypted_message
        self.msg = ""
        for i in self.encrypted:
            if i != '\n' and i != ' ':
                self.msg += chr(ord(i) - 3)
            elif i == '\n':
                self.msg += "\n"
            elif i == ' ':
                self.msg += " "
        return self.msg
