from .encryption import Encryption
import requests
import json

'''
- We have implemented Reverse Encryption Algorithm on your behalf.
- Please integrate with the below end points to implement the encrypt and decrypt
methods
- Postman documentation can be found here :
    https://documenter.getpostman.com/view/469000/SVfRv8vk?version=latest
    - Encryption Endpoint : http://backendtask.robustastudio.com/encode
    - Decryption Endpoint : http://backendtask.robustastudio.com/decode
'''


class ReverseEcryption(Encryption):
    _base_url = "http://backendtask.robustastudio.com/"

    def encode(self, message: str = None) -> str:
        """
        Encrypt message
        :param message: message to be encrypted
        :return: encrypted message
        """
        if message:
            self.msg = message

        _encoding_url = self._base_url + "encode"

        payload = {
            "string": self.msg
        }
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", _encoding_url, headers=headers, data=json.dumps(payload))
        self.encrypted = response.json()['string']
        return self.encrypted

    def decode(self, encrypted_message: str = None) -> str:
        """
        Decrypt Message
        :param encrypted_message: decrypt the encrypted message
        :return: plain message
        """
        if encrypted_message:
            self.encrypted = encrypted_message

        _decoding_url = self._base_url + "decode"
        payload = {
            "string": self.encrypted
        }
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", _decoding_url, headers=headers, data=json.dumps(payload))
        self.msg = response.json()['string']
        return self.msg
