# Three Styles Encryption
[![pipeline status](https://gitlab.com/georgemessiha22/robusta/badges/master/pipeline.svg)](https://gitlab.com/georgemessiha22/robusta/-/commits/master)

This project was a part of Robusta Studios Egypt Interview, following all instructions in DESCRIPTION.md


## Setup project

### Requirements

- python3
- pip 19 for python 3

OR 

- docker
     
### Install on local machine (linux)
#### **OPTIONAL** start virtualenv with python3
```bash
virtualenv --python python3 venv
source venv/bin/activate
```

#### Install requirements using pip:
```bash
pip3 install -r requirements.txt
```
#### run project through:
```bash
python main.py
```

#### Testing:
```bash
python -m unittest
```

### Install Using Docker
#### Build
```bash
docker build -t george_task .
```

#### RUN
```bash
docker run -it george_task
```

#### Test
```bash
docker run -it george_task "bash"
python -m unittest
```
